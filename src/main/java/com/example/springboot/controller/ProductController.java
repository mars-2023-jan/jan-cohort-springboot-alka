package com.example.springboot.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.model.Product;
import com.example.springboot.respository.ProductRepository;

@CrossOrigin(origins = "*")
@RestController // @Controller + @ResponseBody
@RequestMapping("/api")
public class ProductController {

	// @RequestMapping(method=RequestMethod.GET)

	@Autowired
	ProductRepository repository;

	@GetMapping("/SortedProducts")
	public ResponseEntity<List<Product>> getSortedProducts() {
//		System.out.println("Inside Product Controller");
		List<Product> prodList = repository.findAllProductsSorted();
		return new ResponseEntity<>(prodList,HttpStatus.OK);
	}

@PutMapping("/product")
public ResponseEntity<Product> addProduct(@RequestBody Product prod){
	repository.save(prod);
	return new ResponseEntity<>(prod,HttpStatus.CREATED);
}

@GetMapping("/product/{id}")
public ResponseEntity<Product> getProductById(@PathVariable("id") String prodId){
	Optional<Product> prodData = repository.findById(prodId);
	if(prodData.isPresent()) {
		return new ResponseEntity<>(prodData.get(),HttpStatus.OK);
	}
	else {
		return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
				
	}
}


@PutMapping("/product/{id}")
public ResponseEntity<Product> updateProduct(@PathVariable("id") String prodId,@RequestBody Product prod){
	Optional<Product> prodData = repository.findById(prodId);
	if(prodData.isPresent()) {
		Product product = prodData.get();
		product.setProdName(prod.getProdName());
		product.setProdDesc(prod.getProdDesc());
		product.setPrice(prod.getPrice());
		return new ResponseEntity<>(repository.save(product),HttpStatus.OK);
	}
	else {
		return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
	}
	
}

@DeleteMapping("/product/{id}")
public ResponseEntity<Product> deleteProduct(@PathVariable("id") String prodId){
	try {
	repository.deleteById(prodId);
	return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
	}
	catch(Exception e) {
	return new ResponseEntity<Product>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
}

//1. get all the products
//2. add a new product
//3. update a product
//4. delete a product

//Connect to the database using ORM Framework - Hibernate
//create table from java object or map existing table to a java objects
//implementation of JPA and all annotations are part of JPA
//every table in db should be mapped to a java object
//JPA gives annotation and hibernate gives implementation to that
//