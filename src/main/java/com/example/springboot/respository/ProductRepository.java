package com.example.springboot.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.springboot.model.Product;

public interface ProductRepository extends JpaRepository<Product,String>{
	
	@Query(value="SELECT p FROM Product p ORDER BY prodName")
	List<Product> findAllProductsSorted();
}
